//mongoose config
//Need to set datadb path
//mongod --dbpath /Users/minminsanjose/PycharmProjects/NodeProjectGov/data

require('./database');

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cons = require('consolidate');

//new
var mongo = require('mongodb');
var MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server;

var mongoose = require('mongoose');
var monk = require('monk');

var routes = require('./routes/index');
var users = require('./routes/users');

//new
var form = require('./routes/form');
//

var app = express();

//new
app.locals.name = "Sebastian";

//app.engine('html', cons.swig);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


var mongoclient = new MongoClient(new Server('localhost', 27017, { 'native_parser': true }));
//var db = mongoclient.db('course');
var db = monk('localhost:27017/nodetest');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


//app.get('/', function (req, res) {
//  db.collection('hello_mongo_express').findOne({}, function (err, doc) {
//    res.render('hello', doc);
//  });
//});

//Make our db accessible to our router
app.use(function (req, res, next) {
  req.db = db;
  next();
});

app.use('/', routes);
app.use('/users', users);


//new
app.use('/form', form);
app.use('/create', form);
//

//if (app.get('env') === 'development') {
//  //app.use(express.error)
//  mongoose.connect('mongodb://localhost:27017/nodetest');
//}
//
//mongoose.model('userdata', {"name": String, "age": Number, "sex": String, "country": String, "dateCreated": Date});

//app.get('/userdata', function (req, res) {
//  mongoose.model('userdata').find(function (err, userdata) {
//    res.send(userdata);
//  });
//});

//Open the connection to the server
MongoClient.connect('mongodb://localhost:27017/nodetest', function (err, db) {

  if(err) throw err;

  db.collection('userdata').findOne({"name": "Tony Cheung"}, function (err, doc) {

    if(err) throw err;

    //Print the result
    console.dir(doc);

    //Close the DB
    db.close();
  });

  //Declare success
  console.dir("Called findOne!");
});





// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stack traces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
