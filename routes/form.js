//new
var mongoose = require('mongoose');
var Comment = mongoose.model('comments');

var express = require('express');
var router = express.Router();

/* GET form. */
//router.get('/', function (req, res) {
//    //res.send('My funky form');
//    res.render('form', {title: 'Application Complete'});
//});
router.get('/', function (req, res) {
    Comment.find(function (err, comments) {
        console.log(comments)
        res.render(
            'form',
            {title: 'Application Complete', comments: comments}
        );
    });
});


/* POST */
router.post('/', function (req, res) {
    new Comment({title: req.body.comment})
        .save(function (err, comment) {
            console.log(req.body.comment);
            res.redirect('form');
        });

});

module.exports = router;